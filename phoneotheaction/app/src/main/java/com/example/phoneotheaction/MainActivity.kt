package com.example.phoneotheaction

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import androidx.appcompat.widget.AppCompatButton
import com.example.phoneotheaction.R.id.ccp
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.hbb20.CountryCodePicker

class MainActivity() : AppCompatActivity() {



    @SuppressLint("SuspiciousIndentation")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

       val phoneNumber = findViewById<TextInputEditText>(R.id.phoneNumber)
       val ccp = findViewById<CountryCodePicker>(ccp)
          ccp.registerCarrierNumberEditText(phoneNumber)


        val sendOtp=findViewById<AppCompatButton>(R.id.sendOtp)
        sendOtp.setOnClickListener {
            val intent= Intent(this,enterotp::class.java)
           intent.putExtra("phoneNumber",ccp.fullNumberWithPlus.replace(" ",""))
            startActivity(intent)
           finish()
       }
    }
}




