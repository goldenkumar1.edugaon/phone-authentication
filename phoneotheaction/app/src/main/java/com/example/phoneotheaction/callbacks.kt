package com.example.phoneotheaction

import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider

object callbacks:PhoneAuthProvider.OnVerificationStateChangedCallbacks() {


    override fun onCodeSent(verficationId: String, p1: PhoneAuthProvider.ForceResendingToken) {
        GlobalVariable.id = verficationId
    }

    override fun onVerificationCompleted(p0: PhoneAuthCredential) {

    }

    override fun onVerificationFailed(p0: FirebaseException) {

    }

    }
