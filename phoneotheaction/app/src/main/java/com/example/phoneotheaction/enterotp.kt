package com.example.phoneotheaction

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.view.ActionMode.Callback
import androidx.appcompat.widget.AppCompatButton
import com.google.android.material.textfield.TextInputEditText
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import java.util.concurrent.TimeUnit

class enterotp : AppCompatActivity() {
    @SuppressLint("MissingInflatedId")
    lateinit var auth:FirebaseAuth

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enterotp)

       auth=FirebaseAuth.getInstance()

       val getPhoneNumber = intent.getStringExtra("phoneNumber")

        val enterOtp = findViewById<TextInputEditText>(R.id.enterOTP)

        val verifyButton=findViewById<AppCompatButton>(R.id.verifyOtp)
        verifyButton.setOnClickListener {

         val credential=PhoneAuthProvider.getCredential(
             GlobalVariable.id.toString(),enterOtp.text.toString())
         auth.signInWithCredential(credential)
             .addOnSuccessListener {

       val intent=Intent(this,Homepage::class.java)

      startActivity(intent)
      finish()
      }
       }


        val options=PhoneAuthOptions.newBuilder(auth)
        .setPhoneNumber(getPhoneNumber.toString())
        .setTimeout(60L,TimeUnit.SECONDS)
        .setActivity(this)
            .setCallbacks(callbacks)
        .build()
    PhoneAuthProvider.verifyPhoneNumber(options)

    }
}